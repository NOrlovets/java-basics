package main;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Lab {


    public static void main(String[] args) {
        Student one = new Student("Nik", "Java", 56, 555552, LocalDate.of(2017, 6, 27));
        Student two = new Student("Mike", "JS", 56, 555551, LocalDate.of(2017, 6, 27));
        Student three = new Student("John", "Java", 32, 11, LocalDate.of(2017, 6, 27));
        Student four = new Student("Alise", ".NET", 16, 3, LocalDate.of(2017, 6, 27));
        Student five = new Student("Anny", ".NET", 80, 1, LocalDate.of(2017, 6, 27));

        List<Student> students = new ArrayList<>();
        students.add(one);
        students.add(two);
        students.add(three);
        students.add(four);
        students.add(five);

        System.out.println(students.toString());

        Collections.sort(students, Student.AvescoreComparator);
        System.out.println(students.toString());

        Collections.sort(students, Student.LefthoursComparator);
        System.out.println(students.toString());

        List<Student> filteredStu = students;
        filteredStu.removeIf(student -> !student.isKickedout());
        System.out.println(filteredStu.toString());
        //System.out.println(one.getAvescore());
        System.out.println(one.getStartdate());
        //System.out.println(one.isKickedout());
        //System.out.println(one.getLefthours());
    }
}
