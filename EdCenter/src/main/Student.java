package main;

import java.time.LocalDate;
import java.util.Comparator;

public class Student <T> {

    private String name;
    private String group;
    private int hours;
    private int daysgone;
    private int lefthours;
    private double avescore;
    private int scores;
    private boolean kickedout;
    private LocalDate startdate;

    public double nextsum;
    public int i = 1;

    Student(String name, String group, int hours, int scores, LocalDate startdate) {
        this.name = name;
        this.group = group;
        this.hours = hours;
        this.scores = scores;
        this.startdate = startdate;
        daysgone(getScores());
        Avescore(getScores());
        lefthours(getHours());
        kickedout(getAvescore());
    }

    /*@Override
    public int compareTo(Student stu) {
        if (stu.kickedout) return 1;
        else return -1;
    }*/

    public static Comparator<Student> AvescoreComparator = new Comparator<Student>() {

        @Override
        public int compare(Student e1, Student e2) {
            if (e1.getAvescore() < e2.getAvescore()) return -1;
            if (e1.getAvescore() > e2.getAvescore()) return 1;
            return 0;
        }
    };

    public static Comparator<Student> LefthoursComparator = new Comparator<Student>() {

        @Override
        public int compare(Student e1, Student e2) {
            return (e1.getLefthours() - e2.getLefthours());
        }
    };

    /*public static Comparator<Student> KickedoutComparator = new Comparator<Student>() {

        @Override
        public int compare(Student e1, Student e2) {
            return (int) (e1.getAvescore() - e2.getAvescore());
        }
    };*/

    @Override
    public String toString(){
        return "Name: " + getName() + ", Group: " + getGroup() + ", Left hours: "
                + lefthours + ", Avescore: " + avescore + ", KOUT: " + kickedout + "\n";
    }

    public void lefthours(int hours) {
        int lhours = hours - (getDaysgone() * 8);
        setLefthours(lhours);
    }

    public void kickedout(double avescore) {
        double allavescore = 0;
        if (avescore < 4.5) {
            double llhours = getLefthours() / 8;
            allavescore = ((llhours * 5 + nextsum) / (getHours() / 8));
        }
        if ((avescore >= 4.5) || (allavescore >= 4.5))
            setKickedout(false);
        else setKickedout(true);
    }

    public void daysgone(int scores) {
        int temp;
        temp = scores / 10;

        if (temp != 0) {
            i += 1;
            daysgone(temp);
        } else setDaysgone(i);
    }

    public void Avescore(int scores) {
        int temp;
        int sum = 0;
        int numbers = scores;
        int dg = getDaysgone();

        for (int j = 0; j < dg; j += 1) {
            temp = numbers % 10;
            numbers = numbers / 10;
            sum += temp;
        }

        nextsum = sum;
        setAvescore(nextsum / dg);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getLefthours() {
        return lefthours;
    }

    public void setLefthours(int lefthours) {
        this.lefthours = lefthours;
    }

    public double getAvescore() {
        return avescore;
    }

    public void setAvescore(double avescore) {
        this.avescore = avescore;
    }

    public int getScores() {
        return scores;
    }

    public void setScores(int scores) {
        this.scores = scores;
    }

    public boolean isKickedout() {
        return kickedout;
    }

    public void setKickedout(boolean kickedout) {
        this.kickedout = kickedout;
    }

    public int getDaysgone() {
        return daysgone;
    }

    public void setDaysgone(int daysgone) {
        this.daysgone = daysgone;
    }

    public LocalDate getStartdate() {
        return startdate;
    }

    public void setStartdate(LocalDate startdate) {
        this.startdate = startdate;
    }
}
